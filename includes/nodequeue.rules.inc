<?php
// $Id$

/**
 * @file rules integration for the nodequeue module
 *
 * @addtogroup nodequeue
 * @{
 */

require_once('nodequeue.eval.inc');

/**
 * Implements of hook_form_alter().
 */
function nodequeue_rules_form_alter(&$form, $form_state, $form_id) {
  if (strpos($form_id, 'nodequeue_client_form_') !== FALSE) {
    // Add custom submit handler to nodequeue form.
    $form['#submit'][] = 'nodequeue_rules_client_form_submit';
  }
}

/**
 * Custom submit handler for nodequeue submissions.
 */
function nodequeue_rules_client_form_submit($form, &$form_state) {
  global $user;
  // Do stuff
}

/**
 * Implements hook_rules_file_info() on behalf of the nodequeue module.
 */
function nodequeue_rules_file_info() {
  return array('nodequeue_rules.eval');
}

/**
 * Implements hook_rules_event_info() on behalf of the nodequeue module.
 */
function nodequeue_rules_event_info() {
  $defaults = array(
    'group' => t('Nodequeue'),
    'module' => 'nodequeue',
    'access callback' => 'nodequeue_rules_integration_access',
  );

  $args = array(
    'queue' => array(
      'type' => 'queue',
      'label' => t('Queued node'),
    ),
  );

  $items = array(
    'nodequeue_insert' => $defaults + array(
      'label' => t('After saving new queue'),
      'variables' => rules_events_nodequeue_variables(t('created queue')),
      'arguments' => $args,
    ),
    'nodequeue_update' => $defaults + array(
      'label' => t('After updating existing queue'),
      'variables' => rules_events_nodequeue_variables(t('updated queue'), TRUE),
      'arguments' => $args,
    ),
    'nodequeue_presave' => $defaults + array(
      'label' => t('Before saving queue'),
      'variables' => rules_events_nodequeue_variables(t('saved queue'), TRUE),
      'arguments' => array(),
    ),
    'nodequeue_delete' => $defaults + array(
      'label' => t('After deleting a queue'),
      'variables' => rules_events_nodequeue_variables(t('deleted queue')),
      'arguments' => array(),
    ),
  );

  $queues = nodequeue_load_queues(array_keys(nodequeue_get_all_qids()));
  foreach ($queues as $queue) {
    // For each nodequeue we define two events
    $items['nodequeue_added_' . $queue->qid] = $defaults + array(
      'label' => t('Node added to the nodequeue "@queue-title"', array('@queue-title' => $queue->title)),
      'variables' => rules_events_nodequeue_variables(t('added queue @queue-title', array('@queue-title' => $queue->title))),
      'arguments' => $args,
    );

    $items['nodequeue_removed_' . $queue->qid] = $defaults + array(
      'label' => t('Node removed from nodequeue "@queue-title"', array('@queue-title' => $queue->title)),
      'variables' => rules_events_nodequeue_variables(t('deleted queue @queue-title', array('@queue-title' => $queue->title))),
      'arguments' => $args,
    );

    $items['nodequeue_saved_' . $queue->qid] = $defaults + array(
      'label' => t('Nodequeue "@queue-title" is saved', array('@queue-title' => $queue->title)),
      'variables' => rules_events_nodequeue_variables(t('saved queue @queue-title', array('@queue-title' => $queue->title))),
      'arguments' => array(),
    );
  }

  // Specify that on presave the nodequeue is saved anyway.
  $items['nodequeue_presave']['variables']['nodequeue']['skip save'] = TRUE;
  return $items;
}

/**
 * Implements hook_rules_condition_info() on behalf of the nodequeue module.
 */
function nodequeue_rules_condition_info() {
  return array(
    'nodequeue_is_named' => array(
      'label' => t('Nodequeue is named'),
      'help' => t('Evaluates to TRUE if the given queue has the given name.'),
      'base' => 'strstr',//'rules_condition_nodequeue_is_named',
      'group' => t('Nodequeue'),
      'access callback' => 'rules_node_integration_access',
      'parameter' => array(
        'queue' => array(
          'type' => 'list<text>',
          'label' => t('Queue'),
          'options list' => 'nodequeue_rules_get_names',
        ),
      ),
    ),
  );
}

/**
 * Implements hook_rules_action_info() on behalf of the nodequeue module.
 */
function nodequeue_rules_action_info() {
  return array(
    'nodequeue_add_action' => array(
      'label' => t('Add to Nodequeues'),
      'group' => t('Nodequeue'),
      'callbacks' => array(
        'dependencies' => 'nodequeue_rules_dependencies',
//        'help' => 'nodequeue_rules_add_node_help',
      ),
      'access callback' => 'nodequeue_rules_admin_access',
      'base' => 'rules_action_nodequeue_add_node',
      'parameter' => array(
        'node' => array(
          'type' => 'node',
          'label' => t('Node'),
        ),
        'queue' => array(
          'type' => 'list<text>',
          'label' => t('Queue'),
          'options list' => 'nodequeue_rules_get_names',
        ),
        'position' => array(
          'type' => 'integer',
          'label' => t('Position'),
          'optional' => TRUE,
        ),
      ),
    ),
    'nodequeue_remove_action' => array(
      'label' => t('Remove from Nodequeues'),
      'group' => t('Nodequeue'),
      'callbacks' => array(
        'dependencies' => 'nodequeue_rules_dependencies',
//        'help' => 'nodequeue_rules_remove_node_help',
      ),
      'access callback' => 'nodequeue_rules_admin_access',
      'base' => 'rules_action_nodequeue_remove_node',
      'parameter' => array(
        'node' => array(
          'type' => 'node',
          'label' => t('Node'),
        ),
        'queue' => array(
          'type' => 'list<text>',
          'label' => t('Queue'),
          'options list' => 'nodequeue_rules_get_names',
        ),
      ),
    ),
  );
}

/**
 * Returns some parameter suitable for using it with a nodequeue
 */
function rules_events_nodequeue_variables($node_label, $update = FALSE) {
  return array(
    'queue' => array(
      'type' => 'text', 
      'label' => $node_label
    ),
  );
}

/**
 * Callback to specify the path module as dependency.
 */
function nodequeue_rules_dependencies() {
  return array('nodequeue', 'rules');
}

/**
 * Nodequeue integration access callback.
 */
function nodequeue_rules_integration_access($type, $name) {
  if ($type == 'event' || $type == 'condition') {
    return entity_access('view', 'nodequeue');
  }
}

/**
 * Nodequeue integration admin access callback.
 */
function nodequeue_rules_admin_access() {
  return user_access('administer nodequeue');
}

/**
 * Return a list with nodequeue names
 */
function nodequeue_rules_get_names($element, $name = NULL) {
  $items = array();
  $queues = nodequeue_load_queues(nodequeue_get_all_qids(25));
  foreach ($queues as $queue) {
    $items[$queue->qid] = $queue->title;
  }
  return $items;
}

/**
 * @}
 */
